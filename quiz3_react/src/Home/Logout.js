import React, { useContext } from 'react'
import { Redirect } from 'react-router-dom';
import { AuthContext } from './AuthContext';
import Home from '../Home/Home'

const Logout = () => {
    const [, setLogin] = useContext(AuthContext);
    setLogin(false)
    Redirect(Home)

}

export default Logout