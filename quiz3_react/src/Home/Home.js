import React, { Component } from 'react'
import axios from 'axios'

class HomeFilm extends Component {

    state = {
        dataFilm: null
    }

    componentDidMount() {
        if (this.state.dataFilm === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
                .then(res => {
                    const film = res.data
                    console.log(film)
                    this.setState({ dataFilm: film })
                })
        }
    }

    render() {
        const img = {
            width: "50%",
            height: "300px",
        }
        const card = {
            width: "95%",
            margin: "10px",
            "border-radius": "10px",
            "box-shadow": "#aaa"
        }

        const container = {
            padding: "10px",
            "display": "flex"
        }

        return (
            <>
                <div className="outer">
                    <h1>Daftar Film Film Terbaik</h1>
                    {
                        this.state.dataFilm !== null && this.state.dataFilm.map(item => {
                            return (
                                <div style={card}>
                                    <h4>{item.title}</h4>
                                    <div style={container}>
                                        <img src={item.image_url} style={img} alt="Avatar" />
                                        <div className="colum">
                                            <strong style={{ width: "100px" }}>Rating: {item.rating}</strong> <br />
                                            <strong style={{ width: "100px" }}>Durasi:{parseInt(item.duration / 60)} Jam</strong> <br />
                                            <strong style={{ width: "100px" }}>Genre: {item.genre}</strong> <br />
                                        </div>
                                    </div>
                                    <strong style={{ width: "100px" }}>Deskripsi:</strong> {item.description}
                                    <hr />
                                </div>
                            )
                        })
                    }
                </div>
            </>
        )
    }


}

export default HomeFilm