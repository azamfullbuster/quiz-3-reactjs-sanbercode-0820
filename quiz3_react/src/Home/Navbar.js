import React, { useContext } from 'react';
import './Navbar.css';
import logo from '../img/logo.png'
import { AuthContext } from './AuthContext';
import Login from './Login';
import Logout from './Logout'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Home from './Home'
import About from '../About/About'
import Film from '../Film/Film'




const Navbar = () => {
    const [login, , ,] = useContext(AuthContext);

    if (login == false) {
        return (
            <Router>
                <>
                    <header className="light">
                        <img id="logo" src={logo} width="200px" />
                        <nav>
                            <ul className="lightul">
                                <li className="lightli">
                                    <Link to="/Home" className="lighta">Home</Link>
                                </li>
                                <li className="lightli">
                                    <Link to="/About" className="lighta">About</Link>
                                </li>
                                <li className="lightli">
                                    <Link to="/Film" className="lighta">Movie List Editor</Link>
                                </li>
                                <li className="lightli">
                                    <Link to="/Login" className="lighta">Login</Link>
                                </li>
                            </ul>
                        </nav>
                    </header>

                    <Switch>
                        <Route exact path="/Home" component={Home} />
                        <Route exact path="/About" component={About} />
                        <Route exact path="/Film" component={Film} />
                        <Route exact path="/Login" component={Login} />

                    </Switch>

                </>
            </Router>
        )
    }
    else if (login == true) {
        return (
            <Router>
                <>
                    <header className="light">
                        <nav>
                            <ul className="lightul">
                                <li className="lightli">
                                    <Link to="/Home" className="lighta">Home</Link>
                                </li>
                                <li className="lightli">
                                    <Link to="/About" className="lighta">About</Link>
                                </li>
                                <li className="lightli">
                                    <Link to="/Film" className="lighta">Movie List Editor</Link>
                                </li>
                                <li className="lightli">
                                    <Link to="/Logout" className="lighta">Logout</Link>
                                </li>
                            </ul>
                        </nav>
                    </header>

                    <Switch>
                        <Route exact path="/Home" component={Home} />
                        <Route exact path="/About" component={About} />
                        <Route exact path="/Film" component={Film} />
                        <Route exact path="/Logout" component={Logout} />

                    </Switch>

                </>
            </Router>
        )
    }

}



export default Navbar;