import React, { createContext, useState } from 'react';

export const AuthContext = createContext();

export const AuthProvider = props => {
    const [login, setLogin] = useState(false);
    const [auth, setAuth] = useState({
        user: "", pass: ""
    })

    return (
        <AuthContext.Provider value={[login, setLogin, auth, setAuth]}>
            {props.children}
        </AuthContext.Provider>
    )
}