import React from 'react';
import Navbar from './Navbar';
import { AuthProvider } from './AuthContext';

export default function App() {
    return (
        <AuthProvider>
            <Navbar />
        </AuthProvider>
    )
}