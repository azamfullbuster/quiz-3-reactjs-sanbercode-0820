import React, { useContext } from 'react'
import { Redirect } from 'react-router-dom';
import { AuthContext } from './AuthContext';
import Home from '../Home/Home'

const Login = () => {
    const [, setLogin, auth, setAuth] = useContext(AuthContext);
    let notif = ""
    const changeAuth = (event) => {
        var value = event.target.value
        setAuth({ [event.target.name]: value })
    }

    const checkLogin = () => {
        if (auth.user == "admin" && auth.pass == "admin") {
            setLogin(true)
            Redirect(Home)
            setAuth({ user: "", pass: "" })
        }
        else {
            setAuth({ user: "", pass: "" })
            notif = "Login Gagal"
        }
    }

    return (
        <>
            <div className="outer">
                <h1 style={{ textAlign: "center" }}>Login</h1>
                <strong>Username</strong>
                <input type="text" name="user" value={auth.user} onChange={changeAuth} />
                <br />
                <strong>Password</strong>
                <input type="password" name="pass" value={auth.pass} onChange={changeAuth} />
                <br />
                <button className="button4" onClick={checkLogin}>Login</button>
                <br /><br />
                <h1 style={{ textAlign: "center" }}>{notif}</h1>
            </div>
        </>
    )
}

export default Login