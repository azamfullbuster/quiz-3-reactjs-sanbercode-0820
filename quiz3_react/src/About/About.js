import React from 'react'

const About = () => {
    return (
        <>
            <header>
                <title>about</title>
            </header>
            <body >
                <div className="outer">
                    <div className="inner">
                        <h1 style={{ textAlign: "center" }}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                        <ol>
                            <li><strong style={{ width: "100px" }}>Nama:</strong> Muhamad Azam Fuadi</li>
                            <li><strong style={{ width: "100px" }}>Email:</strong> azamfullbuster@gmail.com</li>
                            <li><strong style={{ width: "100px" }}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                            <li><strong style={{ width: "100px" }}>Akun Gitlab:</strong> @azamfullbuster</li>
                            <li><strong style={{ width: "100px" }}>Akun Telegram:</strong> @azamfuadii</li>
                        </ol>
                    </div>
                </div>
                <br />
                <br />
            </body>
        </>
    )
}

export default About