import React, { useState, createContext, useEffect } from "react";
import axios from 'axios'

export const FilmContext = createContext();

export const FilmProvider = props => {
    const [dataFilm, setDataFilm] = useState(null)
    const [input, setInput] = useState({
        id: null, title: "", description: "", search: "",
        year: 2020, duration: 120, genre: "", rating: 0, image_url: ""
    })

    useEffect(() => {
        if (dataFilm === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
                .then(res => {
                    setDataFilm(res.data)
                    console.log(res.data)
                })
        }
    }, [dataFilm]);

    return (
        <FilmContext.Provider value={[dataFilm, setDataFilm, input, setInput]}>
            {props.children}
        </FilmContext.Provider>
    )
}