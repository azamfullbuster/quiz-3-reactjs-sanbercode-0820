import React from "react";
import { FilmProvider } from "./FilmContext";
import FilmList from "./FilmList"
import FilmForm from "./FilmForm"

const Film = () => {
    return (
        <FilmProvider>
            <FilmList />
            <FilmForm />
        </FilmProvider>
    )
}

export default Film

