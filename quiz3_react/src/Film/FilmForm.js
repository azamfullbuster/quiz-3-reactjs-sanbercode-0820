import React, { useContext } from "react";
import axios from 'axios'
import { FilmContext } from "./FilmContext";

const FilmForm = () => {
    const [dataFilm, setDataFilm, input, setInput] = useContext(FilmContext)

    const submitForm = (event) => {
        event.preventDefault()
        var newId = dataFilm.lenght + 1

        if (input.id === null) {
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, {
                id: newId, title: input.title, description: input.description, year: parseInt(input.year),
                duration: parseInt(input.duration), genre: input.genre, rating: parseInt(input.rating), image_url: input.image_url
            })
                .then(res => {
                    var data = res.data
                    setDataFilm([...dataFilm, {
                        id: newId, title: data.title, description: data.description, year: data.year,
                        duration: data.duration, genre: data.genre, rating: data.rating, image_url: data.image_url
                    }])
                    setInput({
                        id: null, title: "", description: "", search: "",
                        year: 2020, duration: 120, genre: "", rating: 0, image_url: ""
                    })

                })
        } else {
            axios.put(`http://backendexample.sanbercloud.com/api/movies/${input.id}`, {
                title: input.title, description: input.description, year: parseInt(input.year),
                duration: parseInt(input.duration), genre: input.genre, rating: parseInt(input.rating), image_url: input.image_url
            })
                .then(res => {
                    var newDataFilm = dataFilm.map(x => {
                        if (x.id === input.id) {
                            x.title = input.title
                            x.description = input.description
                            x.year = input.year
                            x.duration = input.duration
                            x.genre = input.genre
                            x.rating = input.rating
                            x.image_url = input.image_url
                        }
                        return x
                    })
                    setDataFilm([...newDataFilm])
                    setInput({
                        id: null, title: "", description: "", search: "",
                        year: 2020, duration: 120, genre: "", rating: 0, image_url: ""
                    })
                })

        }
    }

    const changeInput = (event) => {
        var value = event.target.value
        setInput({ ...input, [event.target.name]: value })
    }

    return (
        <>
            <div className="outer">
                <h1 style={{ textAlign: "center" }}>Form Data Film</h1>
                <form onSubmit={submitForm}>
                    <table style={{ width: "40%", margin: "0 auto" }}>
                        <tr>
                            <th>Title</th>
                            <td><input required type="text" name="title" value={input.title} onChange={changeInput} /></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><textarea required rows="3" cols="18" name="description" value={input.description} onChange={changeInput} /></td>
                        </tr>
                        <tr>
                            <th>Year</th>
                            <td><input required type="number" min={1980} name="year" value={input.year} onChange={changeInput} /></td>
                        </tr>
                        <tr>
                            <th>Duration</th>
                            <td><input required type="number" name="duration" value={input.duration} onChange={changeInput} /></td>
                        </tr>
                        <tr>
                            <th>Genre</th>
                            <td><input required type="text" name="genre" value={input.genre} onChange={changeInput} /></td>
                        </tr>
                        <tr>
                            <th>Rating</th>
                            <td><input required type="number" min={0} max={10} name="rating" value={input.rating} onChange={changeInput} /></td>
                        </tr>
                        <tr>
                            <th>Image Url</th>
                            <td><textarea required rows="5" cols="33" name="image_url" value={input.image_url} onChange={changeInput} /></td>
                        </tr>
                        <tr>
                            <td><br /></td>
                        </tr>
                        <tr>
                            <td><button className="button4">Submit</button></td>
                        </tr>
                    </table>
                </form>
            </div>
        </>
    )

}

export default FilmForm