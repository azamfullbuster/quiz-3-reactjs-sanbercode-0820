import React, { useContext } from "react";
import axios from 'axios'
import { FilmContext } from "./FilmContext";

const FilmList = () => {
    const [dataFilm, setDataFilm, input, setInput] = useContext(FilmContext)

    const deleteData = (event) => {
        var idFilm = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idFilm}`)
            .then(res => {
                var newDataFilm = dataFilm.filter(x => x.id !== idFilm)
                setDataFilm([...newDataFilm])
            })

    }

    const editData = (event) => {
        var idFilm = parseInt(event.target.value)
        var film = dataFilm.find(x => x.id === idFilm)

        setInput({
            ...input, id: idFilm, title: film.title, description: film.description,
            year: film.year, duration: film.duration, genre: film.genre, rating: film.rating, image_url: film.image_url
        })

    }

    const changeSearch = (event) => {
        var value = event.target.value
        setInput({ [event.target.name]: value })
    }

    const searchFilm = () => {
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setDataFilm(res.data)
                console.log(res.data)
                var newDataFilm = dataFilm.filter(x => x.title.includes(input.search))
                setDataFilm([...newDataFilm])
            })
    }

    return (
        <>
            <div className="outer">
                <input type="text" name="search" value={input.search} onChange={changeSearch} />
                <button className="button4" onClick={searchFilm}>Search</button>
                <h1 style={{ textAlign: "center" }}> Daftar Film</h1>
                <table style={{ width: "50%", margin: "0 auto", height: "50px", textAlign: "left" }}>
                    <thead >
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Year</th>
                            <th>Duration</th>
                            <th>Genre</th>
                            <th>Rating</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody style={{ border: "1px solid" }}>
                        {
                            dataFilm !== null && dataFilm.map((item, index) => {
                                return (
                                    <>
                                        <tr key={item.id} style={{ height: "20px" }}>
                                            <td>{index + 1}</td>
                                            <td>{item.title}</td>
                                            <td >{item.description}</td>
                                            <td>{item.year}</td>
                                            <td>{item.duration}</td>
                                            <td>{item.genre}</td>
                                            <td>{item.rating}</td>
                                            <td style={{ textAlign: "center" }}>
                                                <button value={item.id} onClick={editData} style={{ marginright: "3px", marginLeft: "5px" }}>Edit</button>
                                                <button value={item.id} onClick={deleteData}>Delete</button>
                                            </td>
                                        </tr>
                                        <hr style={{ width: "2300%" }} />
                                    </>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default FilmList