import React from 'react'

const About = (props) => {
    return (
        <>
            <header>
                <title>about</title>
            </header>
            <body >
                <div className="outer">
                    <div className="inner">
                        <h1 style={{ textAlign: "center" }}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                        <ol>
                            <li><strong style={{ width: "100px" }}>Nama:</strong> {props.name}</li>
                            <li><strong style={{ width: "100px" }}>Email:</strong> {props.email}</li>
                            <li><strong style={{ width: "100px" }}>Sistem Operasi yang digunakan:</strong> {props.os}</li>
                            <li><strong style={{ width: "100px" }}>Akun Gitlab:</strong> {props.gitlab}</li>
                            <li><strong style={{ width: "100px" }}>Akun Telegram:</strong> {props.telegram}</li>
                        </ol>
                    </div>
                </div>
                <br />
                <br />
                <a href="/">
                    <button>kembali ke index</button>
                </a>
            </body>
        </>
    )
}

export default About