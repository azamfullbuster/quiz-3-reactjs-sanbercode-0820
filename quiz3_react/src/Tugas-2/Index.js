import React from 'react'
import './tugas2.css'
import logo from './img/logo.png'

const Index = () => {
    return (
        <>
            <body className="body">
                <header className="header">
                    <img id="logo" src={logo} width="200px" />
                    <nav>
                        <ul>
                            <li className="navli"><a href="index.html">Home </a> </li>
                            <li className="navli"><a href="about.html">About </a> </li>
                            <li className="navli"><a href="contact.html">Movie List Editor </a> </li>
                        </ul>
                    </nav>
                </header>
                <section className="section">
                    <h1>Featured Posts</h1>
                    <div id="article-list">
                        <div className="article">
                            <a href=""><h3>Lorem Post 1</h3></a>
                            <p>
                                Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
                        </div>
                        <div className="article">
                            <a href=""><h3>Lorem Post 2</h3></a>
                            <p>
                                Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
                        </div>
                        <div className="article">
                            <a href=""><h3>Lorem Post 3</h3></a>
                            <p>
                                Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
                        </div>
                        <div className="article">
                            <a href=""><h3>Lorem Post 4</h3></a>
                            <p>
                                Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
                        </div>
                        <div className="article">
                            <a href=""><h3>Lorem Post 5</h3></a>
                            <p>
                                Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
                        </div>
                        <div>
                            <a href=""><h3>Lorem Post 5</h3></a>
                            <p>
                                Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
            </p>
                        </div>
                        <div>
                            <a href=""><h3>Lorem Post 5</h3></a>
                            <p>
                                Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
            </p>
                        </div>
                    </div>
                </section>
                <footer className="footer">
                    <h5>copyright &copy; 2020 by Sanbercode</h5>
                </footer>
            </body>
        </>
    )
}

export default Index

