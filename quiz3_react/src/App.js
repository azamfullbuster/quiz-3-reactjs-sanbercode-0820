import React from 'react';
import './App.css';
import bg from './img/pattern.jpg'
import Routes from './Home/Routes'

function App() {
  return (
    <div style={{
      backgroundImage: `url(${bg})`, fontFamily: "Slabo 27px, serif",
      margin: "10px"
    }}>
      <Routes />
      <footer className="foot">
        <h5>copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </div>
  );
}

export default App;
